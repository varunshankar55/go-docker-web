1.1.2:
    Add resource page to see used resources on cluster
    Get user quota from auth plugins
    Add possiblity to open additional ports
    GODWEB-3 add marketplace recipes
    fix logout issue
1.1.1:
    Fix tmpstorage tests
    Manage "enter" keypress on login page
1.1:
    GOD-10 allow use a script with an interactive session, script executed before ssh server
    GODWEB-1 manage multiple instances for constraints

1.0.1: - add swagger API doc/godocker.json
       - use iStatus monitoring in admin interface (etcd, etc.)
       - add apikey renew in API
       - add Temporary local storage (see go-docker)
       - optional *guest* support, i.e. accepting users connecting with Google, GitHub, ... and not in system. Guest will map to a system user for execution.
1.0: first release
