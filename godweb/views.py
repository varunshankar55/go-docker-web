from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound, HTTPNotFound, HTTPForbidden, HTTPUnauthorized, HTTPBadRequest, HTTPServiceUnavailable
from pyramid.security import remember, forget
from pyramid.renderers import render_to_response
from pyramid.response import Response, FileResponse
from pyramid.url import route_url

import sys
import os
import json
import random
import string
import datetime
from datetime import date, timedelta
import time
from copy import deepcopy
import urllib3
import logging
import socket
import hashlib

from bson import json_util
from bson.json_util import dumps
from bson.objectid import ObjectId
from bson.errors import InvalidId

if sys.version_info > (3,):
    import configparser
else:
    import ConfigParser

import copy
import jwt
from jsonschema import validate

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa

from prometheus_client.exposition import generate_latest
from prometheus_client import Gauge, Counter, Histogram

from godocker.utils import is_array_child_task, is_array_task
import godocker.utils as godutils

from pymongo import DESCENDING


def _in_maintenance(request):
    cfg = request.registry.god_config
    maintenance = request.registry.db_redis.get(cfg['redis_prefix']+':maintenance')
    if maintenance is not None and maintenance == 'on':
        return True
    return False

def _get_user(request, user_id):
    '''
    Get user from session or database
    '''
    user = request.registry.db_mongo['users'].find_one({'id': user_id})
    return user

def _auth_user_filter(request, user_id, filter):
    '''
    Add filter on user if connected user is not an admin
    '''
    admins = request.registry.settings['admin'].split(',')
    if user_id in admins:
        return filter
    else:
        filter['user.id'] = user_id
        return filter

def _is_admin(request, user_id):
    '''
    checks if user is an administrator
    '''
    admins = request.registry.settings['admin'].split(',')
    return user_id in admins

def is_authenticated(request):
    # Try to get Authorization bearer with jwt encoded user information
    if request.authorization is not None:
        try:
            (type, bearer) = request.authorization
            secret = request.registry.god_config['secret_passphrase']
            # If decode ok and not expired
            user = jwt.decode(bearer, secret, audience='urn:godocker/api')
            if 'apikey' in user['user']:
                # Check apikey is still valid
                user_db = request.registry.db_mongo['users'].find_one({'id': user['user']['id']})
                if user_db['credentials']['apikey'] != user['user']['apikey']:
                    return None
            return user['user']
        except Exception:
            prometheus_api_inc(request, 'auth_error')
            return None
    user_id = request.authenticated_userid
    if user_id:
        user = request.registry.db_mongo['users'].find_one({'id': user_id})
        return user
    else:
        prometheus_api_inc(request, 'auth_error')
        return None

@view_config(route_name='home')
def my_view(request):
    pyramid_env = os.environ.get('PYRAMID_ENV', 'dev')
    if pyramid_env == 'prod':
        return HTTPFound(request.static_url('godweb:webapp/dist/'))
    else:
        return HTTPFound(request.static_url('godweb:webapp/app/'))


def prometheus_api_inc(request, method):
    '''
    Increment API calls for prometheus
    '''
    try:
        if 'prometheus' not in request.registry.settings or request.registry.settings['prometheus'] == '0':
            return
    except Exception:
        return
    if request.registry.p_api_requests is None:
        request.registry.p_api_requests = Counter('godocker_api_requests',
                                           'Number of API calls', ["method"])
    request.registry.p_api_requests.labels(method).inc()

@view_config(route_name='api_prometheus', renderer='json', request_method='POST')
def api_prometheus(request):
    '''
    .. http:post:: /api/1.0/prometheus

       Push prometheus metrics, admin only via prometheus_key config parameter

       :statuscode 200: no error
    '''
    cfg = request.registry.god_config
    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()

    form = json.loads(body, encoding=request.charset)
    if 'key' not in form or form['key']=='' or form['key'] != cfg['prometheus_key']:
        return HTTPForbidden('Invalid key')
    del form['key']
    for stat in form['stat']:
        #print('Prometheus:Stat:'+stat+':'+str(form[stat]))
        if stat['name'] not in request.registry.p_exporter or request.registry.p_exporter[stat['name']] is None:
            request.registry.p_exporter[stat['name']] = Histogram(stat['name'], stat['name'])
        else:
            request.registry.p_exporter[stat['name']].observe(stat['value'])
    return {}

@view_config(route_name='prometheus', renderer='string', request_method='GET')
def prometheus(request):
    '''
    .. http:get:: /metrics

       Prometheus metrics

       :statuscode 200: no error
    '''
    request.response.content_type = 'text/plain; version=0.0.4; charset=utf-8'
    cfg = request.registry.god_config
    running = request.registry.db_redis.llen(cfg['redis_prefix']+':jobs:running')
    pending = request.registry.db_redis.get(cfg['redis_prefix']+':jobs:queued')
    if pending is None:
        pending = 0
    total = request.registry.db_redis.get(cfg['redis_prefix']+':jobs')
    if total is None:
        total = 0
    if request.registry.p_pending is None:
        request.registry.p_pending = Gauge('godocker_jobs_queued', 'Number of queued jobs')
    request.registry.p_pending.set(int(pending))
    if request.registry.p_running is None:
        request.registry.p_running = Gauge('godocker_jobs_running', 'Number of running jobs')
    request.registry.p_running.set(int(running))
    if request.registry.p_total is None:
        request.registry.p_total = Gauge('godocker_jobs_total', 'Number of total jobs')
    request.registry.p_total.set(int(total))

    return generate_latest()


@view_config(route_name='user_list', renderer='json', request_method='GET')
def user_list(request):
    '''
    .. http:get:: /api/1.0/user (Admin only)

       List users

       :<json dict: user data to update
       :>json dict: user data
       :statuscode 200: no error
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authorized to access this resource')

    if not _is_admin(request, session_user['id']):
        return HTTPUnauthorized('Not authorized to access this resource')

    users = request.registry.db_mongo['users'].find()
    result = []
    for user in users:
        result.append(user)
    return result


@view_config(route_name='user_info', renderer='json', request_method='PUT')
def user_info_edit(request):
    '''
    .. http:put:: /api/1.0/user/(str:id)

       Update logged in user information (self or admin)

       :<json dict: user data to update
       :>json dict: user data
       :statuscode 200: no error
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authorized to access this resource')

    requested_user = request.matchdict['id']
    if not (_is_admin(request, session_user['id']) or session_user['id'] == requested_user):
        return HTTPUnauthorized('Not authorized to access this resource')

    user = request.registry.db_mongo['users'].find_one({'id': requested_user})
    if not user:
        return HTTPNotFound()
    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()
    form = json.loads(body, encoding=request.charset)

    if not 'credentials' in user:
        user['credentials'] = {}

    user['credentials']['apikey'] = form['credentials']['apikey']
    user['credentials']['public'] = form['credentials']['public']

    data_to_update = {
            'credentials.apikey': user['credentials']['apikey'],
            'credentials.public': user['credentials']['public'],
            }

    if _is_admin(request, session_user['id']):
        user['usage']['prio'] = form['usage']['prio']
        data_to_update['usage.prio'] = user['usage']['prio']
        if 'quota_time' in form['usage']:
            user['usage']['quota_time'] = form['usage']['quota_time']
            data_to_update['usage.quota_time'] = user['usage']['quota_time']
        if 'quota_cpu' in form['usage']:
            user['usage']['quota_cpu'] = form['usage']['quota_cpu']
            data_to_update['usage.quota_cpu'] = user['usage']['quota_cpu']
        if 'quota_ram' in form['usage']:
            user['usage']['quota_ram'] = form['usage']['quota_ram']
            data_to_update['usage.quota_ram'] = user['usage']['quota_ram']

    request.registry.db_mongo['users'].update({'id': user['id']},{'$set': data_to_update})

    return user

@view_config(route_name='user_usage', renderer='json', request_method='GET')
def user_usage(request):
    '''
    .. http:get:: /api/1.0/user/(str:id)/usage

       Get user usage (self or admin)

       :>json list: user usage {'cpu','ram','time'}
       :statuscode 200: no error
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authorized to access this resource')

    requested_user = request.matchdict['id']
    if not (_is_admin(request, session_user['id']) or session_user['id'] == requested_user):
        return HTTPUnauthorized('Not authorized to access this resource')

    dt = datetime.datetime.now()
    cfg = request.registry.god_config
    usages = []
    for i in range(0, cfg['user_reset_usage_duration']):
        previous = dt - timedelta(days=i)
        date_key = str(previous.year)+'_'+str(previous.month)+'_'+str(previous.day)
        if request.registry.db_redis.exists(cfg['redis_prefix']+':user:'+requested_user+':cpu:'+date_key):
                cpu = request.registry.db_redis.get(cfg['redis_prefix']+':user:'+requested_user+':cpu:'+date_key)
                ram = request.registry.db_redis.get(cfg['redis_prefix']+':user:'+requested_user+':ram:'+date_key)
                duration = request.registry.db_redis.get(cfg['redis_prefix']+':user:'+requested_user+':time:'+date_key)
                usages.append({'cpu': int(cpu),
                                'ram': int(ram),
                                'time': float(duration),
                                'date': date_key
                            })



    return usages

@view_config(route_name='user_info', renderer='json', request_method='GET')
def user_info(request):
    '''
    .. http:get:: /api/1.0/user/(str:id)

       Get user information (self or admin)

       :>json dict: user data
       :statuscode 200: no error
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authorized to access this resource')

    requested_user = request.matchdict['id']
    if not (_is_admin(request, session_user['id']) or session_user['id'] == requested_user):
        return HTTPUnauthorized('Not authorized to access this resource')

    user = request.registry.db_mongo['users'].find_one({'id': requested_user})
    if not user:
        return HTTPNotFound()

    user['admin'] = False
    if _is_admin(request, user['id']):
        user['admin'] = True
    return user

@view_config(route_name='user_logged', renderer='json', request_method='GET')
def user_logged(request):
    #auth_policy = request.registry.auth_policy
    user = is_authenticated(request)
    if not user:
        return HTTPNotFound('User not logged')
    user['admin'] = False
    if _is_admin(request, user['id']):
        user['admin'] = True
    return user


@view_config(
    context='velruse.AuthenticationComplete',
)
def login_complete_view(request):
    context = request.context
    user_id = None
    if context.profile['preferredUsername']:
        user_id = context.profile['preferredUsername']
    else:
        user_id = context.profile['accounts'][0]['username']

    email = None
    if context.profile['verifiedEmail']:
        email = context.profile['verifiedEmail']
    result = {
        'id': user_id,
        'email': email,
        'provider_type': context.provider_type,
        'provider_name': context.provider_name,
        'profile': context.profile,
        'credentials': context.credentials,
    }
    cfg = request.registry.god_config
    secret = cfg['secret_passphrase']
    token = jwt.encode({'user': result,
                        'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
                        'aud': 'urn:godocker/api'}, secret)
    return HTTPFound(request.static_url('godweb:webapp/'+request.registry.runenv+'/')+"index.html#login?guest_token="+token)

@view_config(route_name='guest_status', renderer='json', request_method='POST')
def guest_status(request):
    '''
    Update status of a user, user is in body
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')
    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()
    form = json.loads(body, encoding=request.charset)
    request.registry.db_mongo['users'].update({'id':  form['id']},{'$set': { 'guest.active': form['guest']['active']}})
    return {}


@view_config(route_name='guest_bind', renderer='json', request_method='POST')
def guest_bind(request):
    cfg = request.registry.god_config
    if 'guest_allow' not in cfg or not cfg['guest_allow']:
        return HTTPForbidden()
    secret = cfg['secret_passphrase']
    user = None
    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()
    form = json.loads(body, encoding=request.charset)
    try:
        decoded_msg = jwt.decode(form['token'], secret, audience='urn:godocker/api')
        user = decoded_msg['user']
    except Exception:
        return HTTPForbidden()

    guest_id = user['id']
    userhash = hashlib.md5(user['id']).hexdigest()
    user['id'] = userhash
    dbuser = request.registry.db_mongo['users'].find_one({'id':  userhash})
    if not dbuser:
        auth_policy = request.registry.auth_policy
        user_bind = auth_policy.get_user(cfg['guest_bind'])
        homedir = None

        if cfg['guest_home_root'] == 'default':
            homedir = os.path.join(user_bind['homeDirectory'], userhash)
        else:
            homedir = os.path.join(cfg['guest_home_root'], userhash)

        private_key= ''
        public_key = ''
        userdb = {
            'id': userhash,
            'last': datetime.datetime.now(),
            'uid': user_bind['uidNumber'],
            'gid': user_bind['gidNumber'],
            'sgids': user_bind['sgids'],
            'homeDirectory': homedir,
            'email': user['email'],
            'credentials': {
                'apikey': ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10)),
                'private': private_key,
                'public': public_key
            },
            'guest': {
                'active': False,
                'is_guest': True,
                'bind': cfg['guest_bind']
            },
            'usage': auth_policy.get_quotas(cfg['guest_bind'], guest=guest_id)
            }
        request.registry.db_mongo['users'].insert(userdb)
        user = {
                  'id' : userdb['id'],
                  'uidNumber': userdb['uid'],
                  'gidNumber': userdb['gid'],
                  'sgids': userdb['sgids'],
                  'email': userdb['email'],
                  'guest': True,
                  'guest_status': dbuser['guest']['active'],
                  'homeDirectory': userdb['homeDirectory']
                }

    headers = remember(request, user['id'])
    request.response.headerlist.extend(headers)
    user['admin'] = False
    user['guest'] = True
    user['guest_status'] = dbuser['guest']['active']
    token = jwt.encode({'user': user,
                        'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
                        'aud': 'urn:godocker/api'}, secret)
    return { 'user': user, 'token': token }

@view_config(route_name='user_bind', renderer='json', request_method='POST')
def user_bind(request):
    prometheus_api_inc(request, 'user_bind')
    auth_policy = request.registry.auth_policy
    user = {}
    try:
        body = request.body
        if sys.version_info >= (3,):
            body = request.body.decode()
        form = json.loads(body, encoding=request.charset)
        uid = form['uid']
        password = form['password']
        user = auth_policy.bind_credentials(uid, password)
        if user == None:
            return HTTPUnauthorized('Invalid credentials')
        dbuser = request.registry.db_mongo['users'].find_one({'id': user['id']})
        if not dbuser:
            #print str(dbuser)
            # Create a SSH user pair key
            #private_key = rsa.generate_private_key(
            #                            public_exponent=65537,
            #                            key_size=2048,
            #                            backend=default_backend()
            #                            )
            #public_key = private_key.public_key()
            # For debug
            private_key= ''
            public_key = ''
            request.registry.db_mongo['users'].insert({
                'id': user['id'],
                'last': datetime.datetime.now(),
                'uid': user['uidNumber'],
                'gid': user['gidNumber'],
                'sgids': user['sgids'],
                'homeDirectory': user['homeDirectory'],
                'email': user['email'],
                'credentials': {
                    'apikey': ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10)),
                    'private': private_key,
                    'public': public_key
                },
                'usage': auth_policy.get_quotas(user['id'])
                }
                )
        else:
            upgrade = False
            if 'sgids' not in dbuser:
                upgrade = True
            if (dbuser['last'] - datetime.datetime.now()).days > 1 or upgrade:
                # Not updated for one day, update use info
                request.registry.db_mongo['users'].update({'id': user['id']},
                    { '$set': {
                            'last': datetime.datetime.now(),
                            'uid': user['uidNumber'],
                            'gid': user['gidNumber'],
                            'sgids': user['sgids'],
                            'homeDirectory': user['homeDirectory'],
                            'email': user['email']
                        }
                    }

                )

        headers = remember(request, user['id'])
        request.response.headerlist.extend(headers)
    except Exception as e:
        logging.error(str(e))
        user = is_authenticated(request)
        if not user:
            return HTTPUnauthorized('Wrong credentials or not logged in')

    user['admin'] = False
    if _is_admin(request, user['id']):
        user['admin'] = True
    secret = request.registry.god_config['secret_passphrase']
    token = jwt.encode({'user': user,
                        'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
                        'aud': 'urn:godocker/api'}, secret)
    return { 'user': user, 'token': token }


@view_config(route_name='admin_maintenance', renderer='json', request_method='GET')
def admin_maintenance(request):
    '''
    .. http:get:: /admin/maintenance

       :>json list: general status and per node status
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    #user = is_authenticated(request)
    #if not user:
    #    return HTTPUnauthorized('Not authenticated or expired')
    #if not _is_admin(request, user['id']):
    #    return HTTPForbidden('Not authorized to access this resource')
    cfg = request.registry.god_config
    status_global = request.registry.db_redis.get(cfg['redis_prefix']+':maintenance')
    if status_global is None:
        status_global = 'off'
    nodes = request.registry.db_redis.hkeys(cfg['redis_prefix']+':maintenance:nodes')
    status_nodes = []
    for node in nodes:
        status_node = request.registry.db_redis.hget(cfg['redis_prefix']+':maintenance:nodes',node)
        status_nodes.append({ 'name': node, 'status': status_node})
    return {'status': {'general': status_global, 'nodes': status_nodes}}


@view_config(route_name='admin_set_maintenance', renderer='json', request_method='POST')
def admin_set_maintenance(request):
    '''
    .. http:post:: /admin/maintenance/(str:node)/(str:status)

       Admin only, set maintenance status for system or a node
       node: *all* for system, else node name
       When system is in maintenance, new tasks will be rejected with a 503 error.
       Existing tasks (running, pending, ...) are not impacted but pending tasks will not be scheduled.

       Note: for the moment, only system maintenance is available, setting a specific node will have
             no impact and should be used for information only. See issue #32.

       :>json list: general status and per node status
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')

    node = request.matchdict['node']
    status = request.matchdict['status']

    if status != 'on' and status !='off':
        return HTTPForbidden('Wrong status, only on or off are valid status')

    cfg = request.registry.god_config
    if node == 'all':
        request.registry.db_redis.set(cfg['redis_prefix']+':maintenance', status)
    else:
        request.registry.db_redis.hset(cfg['redis_prefix']+':maintenance:nodes',node, status)

    return admin_maintenance(request)

@view_config(route_name='admin_status', renderer='json', request_method='GET')
def _admin_status(request):
    '''
    Admin only, get processes status
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')


    cfg = request.registry.god_config
    status_manager = request.registry.status_manager
    if status_manager is None:
        return []
    else:
        return status_manager.status()
    return status


@view_config(route_name='admin_unwatch', renderer='json', request_method='DELETE')
def _admin_unwatch(request):
    '''
    Admin only, unwatch a process
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')
    host_to_unwatch = request.matchdict['id']
    cfg= request.registry.god_config
    request.registry.db_redis.hdel(cfg['redis_prefix']+':procs', host_to_unwatch)
    #request.registry.db_redis.delele(cfg['redis_prefix']+':procs:'+host_to_unwatch)
    return {'msg': host_to_unwatch+' removed from watch list'}


@view_config(route_name='user_logout', renderer='json', request_method='GET')
def user_logout(request):
    #auth_policy = request.registry.auth_policy
    user_id = request.authenticated_userid
    headers = forget(request)
    request.response.headerlist.extend(headers)
    return { 'msg': 'logout' }

@view_config(route_name='api_task_file', renderer='json', request_method='GET')
def api_task_file(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/files/*file

       List task files if *file is empty or a subdirectory of task directory,
        else download file matching *file

       :>json list: List of files and directory in *file if *file is a directory
       ::>octet-stream: File content if *file is a file
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found
       :statuscode 503: service unavailable

    '''
    if _in_maintenance(request):
        return HTTPServiceUnavailable

    prometheus_api_inc(request, 'task_file')
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    filter = {'id': int(request.matchdict['task'])}
    filter = _auth_user_filter(request, user['id'], filter)
    task = request.registry.db_mongo['jobsover'].find_one(filter)
    if task is None:
        return HTTPNotFound('Job does not exist')
    file_path = '/'.join(str(i) for i in request.matchdict['file'])
    god_dir_path = None
    for v in task['container']['volumes']:
        if v['name'] == 'go-docker':
            god_dir_path = v['path']
            break

    if god_dir_path is None:
        raise HTTPNotFound()

    file_path = os.path.join(god_dir_path,
                            file_path)
    if not os.path.exists(file_path):
        raise HTTPNotFound()

    if os.path.isfile(file_path):
        file_size = os.stat(file_path).st_size
        cfg = request.registry.god_config
        max_file_download = 0
        try:
            max_file_download = int(request.registry.settings['max_file_download'])
        except Exception as e:
            logging.warn('max_file_download not set or badly configured in config')
        if max_file_download > 0 and file_size > max_file_download:
            return HTTPUnauthorized('File too large, download not authorized')
        return FileResponse(file_path,
                                request=request,
                                content_type='text/plain')
    file_dir_list = os.listdir(file_path)
    result = []
    for file_or_dir in file_dir_list:
        if file_or_dir == "godocker.sh":
            continue
        if os.path.isfile(os.path.join(file_path, file_or_dir)):
            result.append({'name': file_or_dir,
                            'type': 'file',
                            'size': os.path.getsize(os.path.join(file_path, file_or_dir))})
        else:
            result.append({'name': file_or_dir,
                            'type':
                            'dir'})
    return result


@view_config(route_name='api_usage', renderer='json', request_method='GET')
def api_usage(request):
    '''
    .. http:get:: /api/1.0/usage

       Get framework resource usage

       :>json list: list of nodes with resources
       :statuscode 200: no error
    '''
    executor = request.registry.executor
    return executor.usage()


@view_config(route_name='api_config', renderer='json', request_method='GET')
def api_config(request):
    '''
    .. http:get:: /api/1.0/config

       Get general configuration

       :>json dict: configuration object with volumes etc...
       :statuscode 200: no error
    '''
    cfg= request.registry.god_config
    volumes = []
    for v in cfg['volumes']:
        volumes.append({'name': v['name'], 'acl': v['acl']})
    images = []
    for v in cfg['default_images']:
        is_default = False
        if 'default' in v:
            is_default = v['default']
        images.append({'name': v['name'], 'url': v['url'], 'default': is_default, 'interactive': v['interactive']})
    constraints = []
    for c in cfg['constraints']:
        constraints.append({'name': c['name'], 'value': c['value'].split(',')})
    return {
        'volumes': volumes,
        'cadvisor': {
            'port': cfg['cadvisor_port'],
            'url': cfg['cadvisor_url_part']
        },
        'defaults': {
            'cpu': cfg['defaults_cpu'],
            'ram': cfg['defaults_ram'],
            'allow_root': cfg['allow_root'],
            'allow_user_images': cfg['allow_user_images'],
            'tmpstorage': cfg['plugin_zfs']
        },
        'images': images,
        'live': {
            'status': cfg['live_events'],
            'url': cfg['live_events_url']
        },
        'constraints': constraints,
        'executor_features': request.registry.executor.features(),
        'guest': request.registry.allow_auth
    }


@view_config(route_name='api_project_usage', renderer='json', request_method='GET')
def api_project_usage(request):
    '''
    .. http:get:: /api/1.0/project/(str:id)/usage

       Get project usage (in project or admin)

       :>json list: project usage {'cpu','ram','time'}
       :statuscode 200: no error
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authorized to access this resource')

    requested_project = request.matchdict['project']

    projects = request.registry.db_mongo['projects'].find({'members':session_user['id']})
    user_in_project = False
    for project in projects:
        if project['id'] == requested_project:
            user_in_project = True
            break
    if not (_is_admin(request, session_user['id']) or user_in_project):
        return HTTPUnauthorized('Not authorized to access this resource')

    dt = datetime.datetime.now()
    cfg = request.registry.god_config
    usages = []
    for i in range(0, cfg['user_reset_usage_duration']):
        previous = dt - timedelta(days=i)
        date_key = str(previous.year)+'_'+str(previous.month)+'_'+str(previous.day)
        if request.registry.db_redis.exists(cfg['redis_prefix']+':group:'+requested_project+':cpu:'+date_key):
                cpu = request.registry.db_redis.get(cfg['redis_prefix']+':group:'+requested_project+':cpu:'+date_key)
                ram = request.registry.db_redis.get(cfg['redis_prefix']+':group:'+requested_project+':ram:'+date_key)
                duration = request.registry.db_redis.get(cfg['redis_prefix']+':group:'+requested_project+':time:'+date_key)
                usages.append({'cpu': int(cpu),
                                'ram': int(ram),
                                'time': float(duration),
                                'date': date_key
                            })



    return usages

@view_config(route_name='api_user_apikey', renderer='json', request_method='DELETE')
def api_user_apikey(request):
    '''
    .. http:delete:: /api/1.0/user/{str:user}/apikey

    Generate a new api key

    :>json dict: new apikey
    :statuscode 200: no error
    :statuscode 401: need login/authentication
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authenticated or expired')
    requested_user = request.matchdict['id']
    if not (_is_admin(request, session_user['id']) or session_user['id'] == requested_user):
        return HTTPUnauthorized('Not authorized to access this resource')
    new_token = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10))
    request.registry.db_mongo['users'].update({
                'id': requested_user},
                {'$set': {
                    'credentials.apikey': new_token
                    }
                })
    return {'apikey': new_token}

@view_config(route_name='api_user_project', renderer='json', request_method='GET')
def api_user_project_list(request):
    '''
    .. http:get:: /api/1.0/user/{str:user}/project

    Get the list of projects for user

    :>json list: List of projects
    :statuscode 200: no error
    :statuscode 401: need login/authentication
    :statuscode 403: not authorized
    '''
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authenticated or expired')

    requested_user = request.matchdict['id']
    if not (_is_admin(request, session_user['id']) or session_user['id'] == requested_user):
        return HTTPUnauthorized('Not authorized to access this resource')

    user = request.registry.db_mongo['users'].find_one({'id': requested_user})


    #projects = request.registry.db_mongo['projects'].find({'$elemMatch': { 'members': requested_user}})
    projects = request.registry.db_mongo['projects'].find({'members':requested_user})

    res = []
    for project in projects:
        res.append(project)
    res.append({'id': 'default', 'prio': 50})
    return res

@view_config(route_name='api_project_list', renderer='json', request_method='GET')
def api_project_list(request):
    '''
    .. http:get:: /api/1.0/project

    Get the list of projects (Admin only)

    :>json list: List of projects
    :statuscode 200: no error
    :statuscode 401: need login/authentication
    :statuscode 403: not authorized
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')
    projects = request.registry.db_mongo['projects'].find()
    res = []
    for project in projects:
        res.append(project)
    return res

@view_config(route_name='api_project', renderer='json', request_method='GET')
def api_project_get(request):
    '''
    .. http:get:: /api/1.0/project/{str:project}

    Get project details (Admin only)

    :>json dict:project
    :statuscode 200: no error
    :statuscode 401: need login/authentication
    :statuscode 403: not authorized
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')
    project = request.registry.db_mongo['projects'].find_one({'id': request.matchdict['project']})
    if not project:
        return HTTPNotFound()
    return project

@view_config(route_name='api_project', renderer='json', request_method='DELETE')
def api_project_delete(request):
    '''
    .. http:delete:: /api/1.0/project/{str:project}

    Delete project details (admin only)

    :statuscode 200: no error
    :statuscode 401: need login/authentication
    :statuscode 403: not authorized
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')
    request.registry.db_mongo['projects'].remove({'id': request.matchdict['project']})
    return {}

@view_config(route_name='api_project_list', renderer='json', request_method='POST')
def api_project_create(request):
    '''
    .. http:post:: /api/1.0/project

    Create a project (admin only)

    :<json dict:project
    :statuscode 200: no error
    :statuscode 401: need login/authentication
    :statuscode 403: not authorized
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')
    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()
    form = json.loads(body, encoding=request.charset)
    project_db = request.registry.db_mongo['projects'].find_one({'id': form['id']})
    if project_db is not None:
       return HTTPBadRequest('Project already exists')

    project_quota = request.registry.auth_policy.get_quotas(form['id'], is_group=True)

    project = { 'id': form['id'],
                'owner': form['owner'],
                'volumes': form['volumes'],
                'description': form['description'],
                'members': form['members'],
                'prio': project_quota['prio'],
                'quota_time': project_quota['quota_time'],
                'quota_cpu': project_quota['quota_cpu'],
                'quota_ram': project_quota['quota_ram']}
    if 'prio' in form:
        project['prio'] = form['prio']
    if 'quota_time' in form:
        project['quota_time'] = form['quota_time']
    if 'quota_cpu' in form:
        project['quota_cpu'] = form['quota_cpu']
    if 'quota_ram' in form:
        project['quota_ram'] = form['quota_ram']
    request.registry.db_mongo['projects'].insert(project)
    return project


@view_config(route_name='api_project', renderer='json', request_method='POST')
def api_project_edit(request):
    '''
    .. http:post:: /api/1.0/project/{str:project}

    Update a project (admin only)

    Fields: description, members, prio, quota_time, quota_cpu, quota_ram

    :<json dict:project
    :statuscode 200: no error
    :statuscode 401: need login/authentication
    :statuscode 403: not authorized
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')
    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()
    form = json.loads(body, encoding=request.charset)
    project = { 'description': form['description'],
                'owner': form['owner'],
                'volumes': form['volumes'],
                'members': form['members'],
                'prio': form['prio'],
                'quota_time': form['quota_time'],
                'quota_cpu': form['quota_cpu'],
                'quota_ram': form['quota_ram']}
    request.registry.db_mongo['projects'].update({'id': form['id']}, {'$set': project})
    project['id'] = form['id']
    return project


@view_config(route_name='api_task_token', renderer='json', request_method='GET')
def api_task_token(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/token

    Get a JWT encoded task to securely exchange a task with other services.

    :>json dict: {'token': generated_token}
    :statuscode 200: no error
    :statuscode 401: need login/authentication
    '''
    prometheus_api_inc(request, 'task_token')
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    filter = {'id': int(request.matchdict['task'])}
    filter = _auth_user_filter(request, user['id'], filter)
    task = request.registry.db_mongo['jobs'].find_one(filter)
    if not task:
        task = request.registry.db_mongo['jobsover'].find_one(filter)
    if not task:
        return HTTPNotFound()
    secret = request.registry.god_config['secret_passphrase']
    del task['_id']
    token = jwt.encode({'task': task,
                        'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
                        'aud': 'urn:godocker/api'}, secret)
    return {'token': token}

@view_config(route_name='api_auth', renderer='json', request_method='POST')
def api_auth(request):
    '''
    .. http:post:: /api/1.0/authenticate

       Authenticate user to get an Authorization bearer token used in future API calls

       :<json dict body: JSON credentials {'apikey': xx, 'user': user_id}
       :>json dict: {'token': generated_token}
       :statuscode 200: no error
       :statuscode 401: need login/authentication

    '''
    token = None
    try:
        body = request.body
        if sys.version_info >= (3,):
            body = request.body.decode()
        form = json.loads(body, encoding=request.charset)
        user_id = form['user']
        user = request.registry.db_mongo['users'].find_one({'id': user_id})
        if not user:
            return HTTPUnauthorized('User not defined, connect a first time via the Web interface to create user in the system.')
        if user['credentials']['apikey'] != form['apikey']:
            return HTTPUnauthorized('Wrong credentials or invalid message')
        auth_policy = request.registry.auth_policy
        guest = None
        if 'guest' in user:
            guest = user['guest']
        user = {
            'id': user['id'],
            'admin': _is_admin(request, user_id),
            'uid': user['uid'],
            'gid': user['gid'],
            'sgids': user['sgids'],
            'homeDirectory': user['homeDirectory'],
            'email': user['email'],
            'apikey': user['credentials']['apikey'],

        }
        if guest:
            user['guest'] = guest

        secret = request.registry.god_config['secret_passphrase']
        token = jwt.encode({'user': user,
                            #'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
                            'aud': 'urn:godocker/api'}, secret)
    except Exception as e:
        logging.error(str(e))
        return HTTPUnauthorized('Wrong credentials or invalid message')
    return {'token': token}


@view_config(route_name='api_task_resume', renderer='json', request_method='GET')
def api_task_resume(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/resume

       Resume a suspended task

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found

    '''
    prometheus_api_inc(request, 'task_resume')
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
        #return HTTPForbidden('Not authorized to access this resource')
    filter = {'id': int(request.matchdict['task'])}
    filter = _auth_user_filter(request, user['id'], filter)
    task = request.registry.db_mongo['jobs'].find_one(filter)
    if not task:
        return HTTPNotFound()

    # If kill already requested or job over, do nothing
    if task['status']['secondary'] != godutils.STATUS_SECONDARY_SUSPENDED:
        return task

    task['status']['secondary'] = godutils.STATUS_SECONDARY_RESUME_REQUESTED

    request.registry.db_mongo['jobs'].update({'id': task['id']},
                {'$set': {
                        'status.secondary': task['status']['secondary']
                        }
            })
    cfg = request.registry.god_config
    request.registry.db_redis.rpush(cfg['redis_prefix']+':jobs:resume', dumps(task))
    if 'credentials' in task['user']:
        del task['user']['credentials']
    return task


@view_config(route_name='api_task_monitor', renderer='json', request_method='GET')
def api_task_monitor(request):
    '''
    .. http:get:: /api/1.0/task/(int:container)/monitor

       Get monitoring info for container

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 404: not found

    '''
    prometheus_api_inc(request, 'task_monitor')
    cfg = request.registry.god_config
    task = request.registry.db_mongo['jobs'].find_one({'id': int(request.matchdict['task'])})
    if not task:
        return HTTPNotFound()
    http = urllib3.PoolManager()
    #r = http.request_encode_body('POST', 'http://'+task['container']['meta']['Node']['Name']+':'+str(cfg['cadvisor_port'])+'/api/v1.2/docker/'+task['container']['id'], fields={"num_stats":60,"num_samples":0},encode_multipart=False)
    try:
        r = http.urlopen('POST', 'http://'+task['container']['meta']['Node']['Name']+':'+str(cfg['cadvisor_port'])+'/api/v1.2/docker/'+task['container']['id'], body=json.dumps({"num_stats":cfg['cadvisor_samples'],"num_samples":0}), headers={'Content-Type': 'application/x-www-form-urlencoded'})
    except Exception as e:
        return Response('could not get the container', status_code = 500)
    if r.status != 200:
        return Response('could not get the container', status_code = r.status)
    res = json.loads(r.data)
    res['id'] = task['container']['id']
    return res



@view_config(route_name='api_task_pending', renderer='json', request_method='GET')
def api_task_pending(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/pending

       Switch a task in CREATED status to PENDING

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found

    '''
    prometheus_api_inc(request, 'task_pending')
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
        #return HTTPForbidden('Not authorized to access this resource')
    filter = {'id': int(request.matchdict['task'])}
    filter = _auth_user_filter(request, user['id'], filter)
    task = request.registry.db_mongo['jobs'].find_one(filter)
    if not task:
        return HTTPNotFound()
    if task['status']['primary'] != godutils.STATUS_CREATED:
        return HTTPForbidden('Task not in created status')

    task['status']['primary'] = godutils.STATUS_PENDING
    request.registry.db_mongo['jobs'].update({'id': task['id']},
                {'$set': {
                        'status.primary': task['status']['primary']
                        }
            })
    if 'credentials' in task['user']:
        del task['user']['credentials']
    return task


@view_config(route_name='api_task_reschedule', renderer='json', request_method='GET')
def api_task_reschedule(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/reschedule

       Reschedule a running task

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found
    '''
    prometheus_api_inc(request, 'task_reschedule')
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
        #return HTTPForbidden('Not authorized to access this resource')
    filter = {'id': int(request.matchdict['task'])}
    filter = _auth_user_filter(request, user['id'], filter)
    task = request.registry.db_mongo['jobs'].find_one(filter)
    if not task:
        return HTTPNotFound()
    if is_array_task(task) or is_array_child_task(task):
        return HTTPForbidden()
    # If kill already requested or job over, do nothing
    if task['status']['primary'] != godutils.STATUS_RUNNING:
        return task

    task['status']['secondary'] = godutils.STATUS_SECONDARY_RESCHEDULE_REQUESTED
    request.registry.db_mongo['jobs'].update({'id': task['id']}, {'$set': {
                        'status.secondary': godutils.STATUS_SECONDARY_RESCHEDULE_REQUESTED
    }})

    cfg = request.registry.god_config
    request.registry.db_redis.rpush(cfg['redis_prefix']+':jobs:reschedule', dumps(task))
    if 'credentials' in task['user']:
        del task['user']['credentials']
    return task


@view_config(route_name='api_task_suspend', renderer='json', request_method='GET')
def api_task_suspend(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/suspend

       Suspend a running task

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found
    '''
    prometheus_api_inc(request, 'task_suspend')
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
        #return HTTPForbidden('Not authorized to access this resource')
    filter = {'id': int(request.matchdict['task'])}
    filter = _auth_user_filter(request, user['id'], filter)
    task = request.registry.db_mongo['jobs'].find_one(filter)
    if not task:
        return HTTPNotFound()

    # If kill already requested or job over, do nothing
    if task['status']['secondary'] == godutils.STATUS_SECONDARY_SUSPEND_REQUESTED or task['status']['primary'] == godutils.STATUS_OVER or task['status']['primary'] == godutils.STATUS_PENDING:
        return task

    task['status']['secondary'] = godutils.STATUS_SECONDARY_SUSPEND_REQUESTED

    request.registry.db_mongo['jobs'].update({'id': task['id']},
                {'$set': {
                        'status.secondary': task['status']['secondary']
                        }
            })
    cfg = request.registry.god_config
    request.registry.db_redis.rpush(cfg['redis_prefix']+':jobs:suspend', dumps(task))
    if 'credentials' in task['user']:
        del task['user']['credentials']
    return task

@view_config(route_name='api_user_task_delete', renderer='json', request_method='DELETE')
def api_user_task_delete(request):
    '''
    .. http:delete:: /api/1.0/user/(str:id)/task

       Kill all user tasks (self or admin)

       :>json list: list of task ids
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    prometheus_api_inc(request, 'task_delete')
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authenticated or expired')

    requested_user = request.matchdict['id']
    if not (_is_admin(request, session_user['id']) or session_user['id'] == requested_user):
        return HTTPUnauthorized('Not authorized to access this resource')

    cfg = request.registry.god_config
    killed_tasks = []
    #tasks = request.registry.db_mongo['jobs'].find({'user': {'id': requested_user}})
    tasks = request.registry.db_mongo['jobs'].find({'user.id': requested_user})
    for task in tasks:
        if task['status']['primary'] == 'over':
            continue
        task['status']['secondary'] = godutils.STATUS_SECONDARY_KILL_REQUESTED
        request.registry.db_mongo['jobs'].update({'id': task['id']},
                     {'$set': {
                             'status.secondary': task['status']['secondary']
                             }
                 })
        request.registry.db_redis.rpush(cfg['redis_prefix']+':jobs:kill', dumps(task))
        killed_tasks.append(task['id'])

    return killed_tasks


@view_config(route_name='api_task', renderer='json', request_method='PUT')
def api_task(request):
    '''
    .. http:put:: /api/1.0/task/(str:id)

       Update a task, only requirements dynamic fields are supported dynamic_fields in config file.
       Other parameters cannot be modified once task is create

       :>json list: Update object with name/value pair, example {"name" : "maxlifespan", "value": "6d"}
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    prometheus_api_inc(request, 'task_update')
    session_user = is_authenticated(request)
    if not session_user:
        return HTTPUnauthorized('Not authenticated or expired')


    filter = {'id': int(request.matchdict['task'])}
    filter = _auth_user_filter(request, session_user['id'], filter)
    task = request.registry.db_mongo['jobs'].find_one(filter)
    if not task:
        return HTTPNotFound()

    cfg = request.registry.god_config
    if 'dynamic_fields' not in cfg:
        return HTTPNotFound()

    requirements = {}
    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()
    update_field = json.loads(body, encoding=request.charset)
    is_admin = _is_admin(request, session_user['id'])
    found = False
    for dynamic_field in cfg['dynamic_fields']:
        if update_field['name'] == dynamic_field['name']:
            if dynamic_field['admin_only'] == True and is_admin:
                request.registry.db_mongo['jobs'].update({'id': task['id']}, {'$set': {'requirements.'+update_field['name'] : update_field['value']}})
            elif dynamic_field['admin_only'] == False and (is_admin or task['user']['id'] == session_user['id']):
                request.registry.db_mongo['jobs'].update({'id': task['id']}, {'$set': {'requirements.'+update_field['name'] : update_field['value']}})
            else:
                return HTTPUnauthorized('Not allowed to modify parameter')
            found = True
            break
    if not found:
        return HTTPNotFound('Field not found or allowed')
    if 'credentials' in task['user']:
        del task['user']['credentials']
    return task


@view_config(route_name='api_task', renderer='json', request_method='DELETE')
def api_task_kill(request):
    '''
    .. http:delete:: /api/1.0/task/(int:task)

       Kill a running task

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found

    '''
    prometheus_api_inc(request, 'task_kill')
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
        #return HTTPForbidden('Not authorized to access this resource')
    filter = {'id': int(request.matchdict['task'])}
    filter = _auth_user_filter(request, user['id'], filter)
    task = request.registry.db_mongo['jobs'].find_one(filter)
    if not task:
        return HTTPNotFound()

    # If kill already requested or job over, do nothing
    if task['status']['primary'] == 'over':
        return task

    task['status']['secondary'] = godutils.STATUS_SECONDARY_KILL_REQUESTED

    request.registry.db_mongo['jobs'].update({'id': task['id']},
                {'$set': {
                        'status.secondary': task['status']['secondary']
                        }
            })
    cfg = request.registry.god_config
    request.registry.db_redis.rpush(cfg['redis_prefix']+':jobs:kill', dumps(task))
    if 'credentials' in task['user']:
        del task['user']['credentials']
    return task

@view_config(route_name='api_task', renderer='json', request_method='GET')
def api_task_get(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)

       Get a task

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found

    '''
    prometheus_api_inc(request, 'task_get')
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
        #return HTTPForbidden('Not authorized to access this resource')
    filter = {'id': int(request.matchdict['task'])}
    filter = _auth_user_filter(request, user['id'], filter)
    task = request.registry.db_mongo['jobs'].find_one(filter)
    if not task:
        task = request.registry.db_mongo['jobsover'].find_one(filter)
    if not task:
        return HTTPNotFound()
    if 'credentials' in task['user']:
        del task['user']['credentials']
    return task

@view_config(route_name='api_task_status', renderer='json', request_method='GET')
def api_task_status(request):
    '''
    .. http:get:: /api/1.0/task/(int:task)/status

       Get a task status

       :>json dict: Task
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
       :statuscode 404: not found

    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
        #return HTTPForbidden('Not authorized to access this resource')
    filter = {'id': int(request.matchdict['task'])}
    filter = _auth_user_filter(request, user['id'], filter)
    task = request.registry.db_mongo['jobs'].find_one(filter)
    if not task:
        task = request.registry.db_mongo['jobsover'].find_one(filter)
    if not task:
        return HTTPNotFound()

    return {'id': task['id'], 'status': task['status'] }


def validate_task(task):
    schema = { "type": "object", "properties":
                {
                   'meta': {
                       "type": "object",
                       "properties": {
                           'name': {"type" : "string"},
                           'description': {"type" : "string"},
                           'tags': {"items": { "type": "string"}}
                       }
                   },
                   'requirements': {
                       'type': 'object',
                       'properties': {
                           'cpu': {"type" : "number"},
                           'ram': {"type" : "number"}
                       }
                   },
                   'container': {
                       'type': 'object',
                       'properties': {
                           'image': {"type" : "string"},
                           'volumes': {"items" : {
                                            "type": "object",
                                            "properties": {
                                                'name': {"type" : "string"},
                                                'acl': {"type" : "string"}
                                            }
                                        }},
                           'network': {"type" : "boolean"},
                           'meta': {"type" : "null"}, # Contains meta information provided by executor (hostname, ...)
                           'root': {"type" : "boolean"}
                       }
                   },
                   'command': {
                       'type': 'object',
                       'properties': {
                           'interactive': {"type" : "boolean"},
                           'cmd': {"type" : "string"}
                       }
                   },
                   'status': {
                       'type': 'object',
                       'properties': {
                           'primary': {"type" : ["string" ,"null"]},
                           'secondary': {"type" : ["string", "null"]}
                       }
                   }
               }
               ,"required": ["meta", "requirements", "command", "container"]
       }

    validate(task, schema)

@view_config(route_name='api_tasks', renderer='json', request_method='POST')
def api_task_add(request):
    '''
    .. http:post:: /api/1.0/task

       Create a new task for user

       :<json dict body: JSON task to add
       :>json dict: {'msg': 'Task added', 'id': task_id}
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 503: service unavailable
    '''
    if _in_maintenance(request):
        return HTTPServiceUnavailable

    prometheus_api_inc(request, 'task_add')
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')


    cfg = request.registry.god_config

    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()
    task = json.loads(body, encoding=request.charset)
    try:
        validate_task(task)
    except Exception as e:
        logging.error(str(e))
        return HTTPBadRequest('Wrong task format')
    if 'status' not in task:
        task['status'] = { 'primary': None, 'secondary': None, 'exitcode': None}
    if not task['status']['primary']:
        task['status']['primary'] = godutils.STATUS_PENDING
        task['status']['secondary'] = None
        task['status']['reason'] = None
        task['status']['exitcode'] = None

    if 'notify' not in task:
        task['notify'] = { 'email': False }

    if task['container']['image'] is None or not task['container']['image']:
        for vol in cfg['default_images']:
            if 'default' in vol and vol['default']:
                task['container']['image'] = vol['url']
                break
        if task['container']['image'] is None:
            return HTTPBadRequest('No image specified and no default image available')

    if not cfg['allow_user_images']:
        allowed = False
        for vol in cfg['default_images']:
            image_name = task['container']['image'].split(':')[0]
            if image_name == vol['name']:
                allowed = True
                break
        if not allowed:
            return HTTPBadRequest('Requested image is not allowed')

    dt = datetime.datetime.now()
    task['date'] = time.mktime(dt.timetuple())
    task['status']['date_over'] = None

    auth_policy = request.registry.auth_policy

    if not 'user' in task:
        task['user']= {}

    if not task['command']['cmd'].startswith("#!"):
        task['command']['cmd'] = "#!/bin/bash\n"+ task['command']['cmd']

    user_from_db = _get_user(request, user['id'])

    if 'guest' in user_from_db and user_from_db['guest']:
        if not user_from_db['guest']['active']:
            return HTTPUnauthorized('Your guest status is not active')
        else:
            logging.debug("Guest access")
            task['user']['guest'] = True
        if not cfg['guest_allow_root']:
            task['container']['root'] = False
        if cfg['guest_home_root'] is None:
            # Remove home if requested
            index = -1
            counter = 0
            for volume in task['container']['volumes']:
                if volume['name'] == 'home':
                    index = counter
                    break
                counter += 1
            if index > -1:
                task['container']['volumes'].pop(index)


    if  _is_admin(request, user['id']):
        if 'id' not in task['user']:
            task['user']['id'] = user['id']
    else:
        task['user']['id'] = user['id']

    task['user']['uid'] = user_from_db['uid']
    task['user']['gid'] = user_from_db['gid']
    task['user']['sgids'] = user_from_db['sgids']

    if 'email' not in task['user'] or task['user']['email'] is None:
        if 'email' not in user_from_db:
            task['user']['email']  = None
        else:
            task['user']['email'] = user_from_db['email']

    task['user']['credentials'] = {
        'apikey': user_from_db['credentials']['apikey'],
        'public': user_from_db['credentials']['public']
    }

    if 'usage' not in user_from_db:
        user_from_db['usage'] = {}

    if 'rate_limit' in cfg and cfg['rate_limit'] is not None:
        current_rate = request.registry.db_redis.get(cfg['redis_prefix'] +
                                   ':user:' + str(task['user']['id'])+ ':rate')
        if current_rate is not None and int(current_rate) >= cfg['rate_limit']:
            return HTTPForbidden('User rate limit reached: ' +
                                 str(cfg['rate_limit']))

    if 'rate_limit_all' in cfg and cfg['rate_limit_all'] is not None:
        current_rate = request.registry.db_redis.get(cfg['redis_prefix'] +
                                   ':jobs:queued')
        if current_rate is not None and int(current_rate) >= cfg['rate_limit_all']:
            return HTTPForbidden('Global rate limit reached: ' + str(cfg['rate_limit_all']) + ', please retry later')

    if 'quota_time' in user_from_db['usage']:
        task['requirements']['user_quota_time'] = user_from_db['usage']['quota_time']
    else:
        task['requirements']['user_quota_time'] = 0

    if 'quota_cpu' in user_from_db['usage']:
        task['requirements']['user_quota_cpu'] = user_from_db['usage']['quota_cpu']
    else:
        task['requirements']['user_quota_cpu'] = 0

    if 'quota_ram' in user_from_db['usage']:
        task['requirements']['user_quota_ram'] = user_from_db['usage']['quota_ram']
    else:
        task['requirements']['user_quota_ram'] = 0


    if 'tmpstorage' not in task['requirements']:
        task['requirements']['tmpstorage'] = None
    else:
        if task['requirements']['tmpstorage'] is not None:
            task['requirements']['tmpstorage']['path'] = None

    if 'project' not in task['user']:
        task['user']['project'] = 'default'

    project = None
    if task['user']['project'] != 'default':
        project = request.registry.db_mongo['projects'].find_one({'id': task['user']['project']})
        if not project or task['user']['id'] not in project['members']:
            return HTTPForbidden('Project not valid')
        task['requirements']['project_quota_time'] = project['quota_time']
        task['requirements']['project_quota_cpu'] = project['quota_cpu']
        task['requirements']['project_quota_ram'] = project['quota_ram']
    else:
        task['requirements']['project_quota_time'] = 0
        task['requirements']['project_quota_cpu'] = 0
        task['requirements']['project_quota_ram'] = 0

    volumes = auth_policy.get_volumes(user_from_db, task['container']['volumes'],task['container']['root'])

    if project is not None:
        for project_volume in project['volumes']:
            # Add project volumes
            if task['container']['root']:
                project_volume['acl'] = 'ro'
            volumes.append(project_volume)

    task['container']['volumes'] = volumes

    task['parent_task_id'] = None

    if 'ports' in task['requirements']:
        try:
            for port in task['requirements']['ports']:
                port = int(port)
        except Exception as e:
            return HTTPBadRequest('Invalid ports')


    task_id = _add_task(request, task)

    return {'msg': 'Task added', 'id': task_id}

def _add_task(request, task):
        '''
        Add a new task, if status not set, set it to pending

        Automatically atribute an id to the task

        :param task: Task to insert
        :type task: dict
        :return: task id
        '''
        cfg = request.registry.god_config
        task_id = request.registry.db_redis.incr(cfg['redis_prefix'] + ':jobs')
        request.registry.db_redis.incr(cfg['redis_prefix'] + ':jobs:queued')
        request.registry.db_redis.incr(cfg['redis_prefix'] + ':user:' + str(task['user']['id'])+ ':rate')
        task['id'] = task_id
        if not task['status']['primary']:
            task['status']['primary'] = godutils.STATUS_PENDING

        if not cfg['allow_root']:
            task['container']['root'] = False

        if 'array' not in task['requirements']:
            task['requirements']['array'] = { 'values': None}

        task['requirements']['array']['task_id']= None
        task['requirements']['array']['nb_tasks']= None
        task['requirements']['array']['nb_tasks_over']= None
        task['requirements']['array']['tasks']= []


        if 'tasks' not in task['requirements']:
            task['requirements']['tasks'] = []

        if is_array_task(task):
            task['requirements']['array']['nb_tasks'] = 0
            task['requirements']['array']['nb_tasks_over'] = 0
            task['requirements']['array']['tasks'] = []
            array_req = task['requirements']['array']['values'].split(':')
            array_first = 0
            array_last = 0
            array_step = 1
            if len(array_req) == 1:
                array_first = 1
                array_last = int(array_req[0])
                array_step = 1
            else:
                array_first = int(array_req[0])
                array_last = int(array_req[1])
                if len(array_req) == 3:
                    array_step = int(array_req[2])
            for i in xrange(array_first, array_last + array_step, array_step):
                subtask = deepcopy(task)
                subtask['requirements']['array']['nb_tasks'] = 0
                subtask['requirements']['array']['tasks'] = []
                subtask['requirements']['array']['task_id'] = i
                subtask['parent_task_id'] = task['id']
                subtask['requirements']['array']['values'] = None
                subtask_id = _add_task(request, subtask)
                task['requirements']['array']['nb_tasks'] += 1
                task['requirements']['array']['tasks'].append(subtask_id)
            request.registry.db_redis.set(cfg['redis_prefix']+':job:'+str(task['id'])+':subtask', task['requirements']['array']['nb_tasks'])
        # Increment image usage counter
        request.registry.db_redis.hincrby(cfg['redis_prefix']+':images', task['container']['image'], 1)

        request.registry.db_mongo['jobs'].insert(task)
        return task_id

@view_config(route_name='api_images', renderer='json', request_method='GET')
def api_images(request):
    '''
    .. http:get:: /api/1.0/image

        List used images

       :>json dict: List of used images
       :statuscode 200: no error
    '''
    cfg = request.registry.god_config
    return request.registry.db_redis.hkeys(cfg['redis_prefix']+':images')

@view_config(route_name='api_image_count', renderer='json', request_method='GET')
def api_image_count(request):
    '''
    .. http:get:: /api/1.0/image/(str:image)

        List used images

       :>json dict: List of used images
       :statuscode 200: no error
    '''
    cfg = request.registry.god_config
    image = request.matchdict['image']
    return { 'image': image, 'count': request.registry.db_redis.hget(cfg['redis_prefix']+':images', image) }


@view_config(route_name='api_task_active', renderer='json', request_method='GET')
def api_task_active(request):
    '''
    .. http:get:: /api/1.0/task/active

        List running or pending tasks for user

       :>json dict: List of tasks
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    prometheus_api_inc(request, 'task_list_active')
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
        #return HTTPForbidden('Not authorized to access this resource')

    filter = {}
    #filter = _auth_user_filter(request, user['id'], filter)
    filter['user.id'] = user['id']

    tasks = request.registry.db_mongo['jobs'].find(filter)
    res = []
    for task in tasks:
        if 'credentials' in task['user']:
            del task['user']['credentials']
        res.append(task)
    return res

@view_config(route_name='api_task_active_all', renderer='json', request_method='GET')
def api_task_active_all(request):
    '''
    .. http:get:: /api/1.0/task/active/all

        List running or pending tasks for all users
        Restricted to administrators

       :>json dict: List of tasks
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')

    filter = {}

    tasks = request.registry.db_mongo['jobs'].find(filter)
    res = []
    for task in tasks:
        if 'credentials' in task['user']:
            del task['user']['credentials']
        res.append(task)
    return res


@view_config(route_name='api_task_over', renderer='json', request_method='GET')
def api_task_over(request):
    '''
    .. http:get:: /api/1.0/task/over

        List last 100 finished tasks

       :>json dict: List of tasks
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    prometheus_api_inc(request, 'task_list_over')
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
        #return HTTPForbidden('Not authorized to access this resource')

    filter = {}
    filter['user.id'] = user['id']
    tasks = request.registry.db_mongo['jobsover'].find(filter, sort=[('id', DESCENDING)], limit=100)
    res = []
    for task in tasks:
        if 'credentials' in task['user']:
            del task['user']['credentials']
        res.append(task)
    return res

@view_config(route_name='api_task_over_all', renderer='json', request_method='GET')
def api_task_over_all(request):
    '''
    .. http:get:: /api/1.0/task/over/all

        List over for all users (last 100)
        Restricted to administrators

       :>json dict: List of tasks
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')

    filter = {}

    tasks = request.registry.db_mongo['jobsover'].find(filter, sort=[('id', DESCENDING)], limit=100)
    res = []
    for task in tasks:
        if 'credentials' in task['user']:
            del task['user']['credentials']
        res.append(task)
    return res


@view_config(route_name='api_task_over_cursor', renderer='json')
def api_task_over_cursor(request):
    '''
    .. http:post:: /api/1.0/task/over/(int:skip)/(int:limit)

        Get finished tasks

        Params:
          skip: number of records to skip in search,
          limit: maximum number of records to return

        Json optional filter: { 'start': timestamp, 'end': timestamp }

       :<json dict: Optional filter on task start date (timestamp)
       :>json dict: List of tasks
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    limit = int(request.matchdict['limit'])
    skip = int(request.matchdict['skip'])
    if request.body:
        date_filter = json.loads(request.body, encoding=request.charset)
    else:
        date_filter = {}
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')

    filter = {}
    #filter = _auth_user_filter(request, user['id'], filter)
    filter['user.id'] = user['id']
    if 'start' in date_filter and 'end' in date_filter:
        filter['date'] = { '$gte' : date_filter['start'], '$lte' : date_filter['end']}
    else:
        if 'start' in date_filter:
            filter['date'] = { '$gte' : date_filter['start']}
        if 'end' in date_filter:
            filter['date'] = { '$lte' : date_filter['end']}
    tasks = request.registry.db_mongo['jobsover'].find(filter, sort=[('id', DESCENDING)], skip=skip, limit=limit)
    res = []
    for task in tasks:
        if 'credentials' in task['user']:
            del task['user']['credentials']
        res.append(task)
    return res

@view_config(route_name='api_task_over_all_cursor', renderer='json')
def api_task_over_all_cursor(request):
    '''
    .. http:post:: /api/1.0/task/over/all/(int:skip)/(int:limit)

        Get finished tasks for all users (Administrator only)

        Params:
          skip: number of records to skip in search,
          limit: maximum number of records to return

        Json optional filter: { 'start': timestamp, 'end': timestamp }

       :<json dict: Optional filter on task start date (timestamp)
       :>json dict: List of tasks
       :statuscode 200: no error
       :statuscode 401: need login/authentication
       :statuscode 403: not authorized
    '''
    limit = int(request.matchdict['limit'])
    skip = int(request.matchdict['skip'])
    if request.body:
        date_filter = json.loads(request.body, encoding=request.charset)
    else:
        date_filter = {}
    user = is_authenticated(request)
    if not user:
        return HTTPUnauthorized('Not authenticated or expired')
    if not _is_admin(request, user['id']):
        return HTTPForbidden('Not authorized to access this resource')

    filter = {}
    if 'start' in date_filter and 'end' in date_filter:
        filter['date'] = { '$gte' : date_filter['start'], '$lte' : date_filter['end']}
    else:
        if 'start' in date_filter:
            filter['date'] = { '$gte' : date_filter['start']}
        if 'end' in date_filter:
            filter['date'] = { '$lte' : date_filter['end']}
    tasks = request.registry.db_mongo['jobsover'].find(filter, sort=[('id', DESCENDING)], skip=skip, limit=limit)
    res = []
    for task in tasks:
        if 'credentials' in task['user']:
            del task['user']['credentials']
        res.append(task)
    return res

@view_config(route_name='api_count', renderer='json', request_method='GET')
def api_count_running(request):
    '''
    .. http:get:: /api/1.0/task/(str:status)/count

        Count number of tasks for all users

        status request parameter should be in [ 'pending', 'running', 'kill', 'all']

       :>json dict: {'total': number of tasks, 'status': requested status}
       :statuscode 200: no error
    '''
    status = request.matchdict['status']
    res = 0
    cfg = request.registry.god_config
    if status == 'running':
        res = request.registry.db_redis.llen(cfg['redis_prefix']+':jobs:running')
    elif status == 'pending':
        res = request.registry.db_redis.get(cfg['redis_prefix']+':jobs:queued')
    elif status == 'kill':
        res = request.registry.db_redis.llen(cfg['redis_prefix']+':jobs:kill')
    elif status == 'all':
        res = request.registry.db_redis.get(cfg['redis_prefix']+':jobs')
        if res is None:
            res = 0

    return {'total': int(res), 'status': status}

@view_config(route_name='api_ping', renderer='json', request_method='GET')
def api_ping(request):
    '''
    .. http:get:: /api/1.0/ping

       Ping web site

       :>json dict:  {'msg': 'pong'}
       :statuscode 200: no error
    '''
    cfg = request.registry.god_config
    status_manager = request.registry.status_manager
    if status_manager is not None:
        hostname = socket.gethostbyaddr(socket.gethostname())[0]
        proc_name = 'web-'+self.hostname
        status_manager.keep_alive(proc_name, 'web')

    return {'msg': 'pong'}

# Marketplace
#api_marketplace_recipes
@view_config(route_name='api_marketplace_recipes', renderer='json', request_method='GET')
def api_marketplace_recipes(request):
    '''
    .. http:get:: /api/1.0/marketplace/recipe

        Get public recipes and user private recipes if logged

       :>json list: {'id': recipe_id, 'description': recipe_description}
       :statuscode 200: no error
    '''
    user = is_authenticated(request)
    recipe_list = []
    recipe_filter = {}
    owner_only = False
    try:
        my_param = request.matchdict['my']
        if int(my_param) == 1:
            owner_only = True
    except Exception as e:
        owner_only = False
    if owner_only:
        if user:
            recipe_filter = {'owner': user['id']}
        else:
            return HTTPForbidden()
    else:
        if not user:
            recipe_filter = {'visible': True}
        elif not _is_admin(request, user['id']):
            recipe_filter = { '$or': [ { 'visible': True }, { 'owner': user['id'] } ] }

    recipes = request.registry.db_mongo['recipes'].find(recipe_filter)
    for recipe in recipes:
        recipe_list.append(recipe)
    return recipe_list

#api_marketplace_recipe, GET/POST/PU/DELETE /api/1.0/marketplace/recipe/{str:recipe}
@view_config(route_name='api_marketplace_recipe', renderer='json', request_method='GET')
def api_marketplace_recipe(request):
    '''
    .. http:get:: /api/1.0/marketplace/recipe/{str:recipe}

        Get a public or owned recipe

       :>json dict: {'id': recipe_id, 'description': recipe_description, 'task': task_originating_id}
       :statuscode 200: no error
       :statuscode 403: not authorized
       :statuscode 404: not found
    '''
    recipe_id = request.matchdict['recipe']
    recipe_filter = {'id': recipe_id}
    user = is_authenticated(request)
    recipe = request.registry.db_mongo['recipes'].find_one(recipe_filter)
    if recipe:
        if not recipe['visible']:
            if recipe['owner'] != user['id']:
                return HTTPForbidden()
        return recipe
    else:
        return HTTPNotFound()


@view_config(route_name='api_marketplace_recipe', renderer='json', request_method='POST')
def api_marketplace_recipe_new(request):
    '''
    .. http:post:: /api/1.0/marketplace/recipe/{str:recipe}

        Create a recipe. Post body must contain description and task elements

       :statuscode 200: no error
       :statuscode 403: not authorized
       :statuscode 404: not found
    '''
    recipe_id = request.matchdict['recipe']
    recipe_filter = {'id': recipe_id}
    user = is_authenticated(request)
    if not user:
        return HTTPForbidden()
    recipe = request.registry.db_mongo['recipes'].find_one(recipe_filter)
    if recipe:
        if recipe['owner'] != user['id']:
            return HTTPForbidden('recipe already exists')

    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()

    form = json.loads(body, encoding=request.charset)
    if 'description' not in form or not form['description']:
        return HTTPForbidden('missing description')
    if 'id' not in form or not form['id']:
        return HTTPForbidden('missing name')
    if 'visible' not in form:
        form['visible'] = True
    else:
        if form['visible'] == 'true':
            form['visible'] = True
        else:
            form['visible'] = False

    if 'task' not in form:
        form['task'] = None

    if recipe:
        request.registry.db_mongo['recipes'].update({'id': recipe_id}, {
            '$set': {
                'description': form['description'],
                'task': form['task'],
                'owner': user['id'],
                'visible': form['visible']
            }
        })
        return {'msg': 'recipe create', 'id': recipe_id}
    else:
        request.registry.db_mongo['recipes'].insert({
            'id': recipe_id,
            'description': form['description'],
            'task': form['task'],
            'owner': user['id'],
            'visible': form['visible']
        })
        return {'msg': 'recipe create', 'id': recipe_id}

@view_config(route_name='api_marketplace_recipe', renderer='json', request_method='PUT')
def api_marketplace_recipe_edit(request):
    '''
    .. http:put:: /api/1.0/marketplace/recipe/{str:recipe}

        Update an owned recipe. Put body can contain description and/or task elements

       :statuscode 200: no error
       :statuscode 403: not authorized
       :statuscode 404: not found
    '''
    recipe_id = request.matchdict['recipe']
    recipe_filter = {'id': recipe_id}
    user = is_authenticated(request)
    if not user:
        return HTTPForbidden()
    recipe = request.registry.db_mongo['recipes'].find_one(recipe_filter)
    if not recipe:
        return HTTPNotfound()

    if recipe['owner'] != user['id']:
        return HTTPForbidden()

    body = request.body
    if sys.version_info >= (3,):
        body = request.body.decode()

    form = json.loads(body, encoding=request.charset)

    updates = {'owner': user['id']}
    if 'description' in form and form['description']:
        updates['description'] = form['description']
    if 'task' in form and form['task']:
        updates['task'] = form['task']

    request.registry.db_mongo['recipes'].update({'id': recipe_id}, {'$set': updates})
    return {'msg': 'recipe updated', 'id': recipe_id}


@view_config(route_name='api_marketplace_recipe', renderer='json', request_method='DELETE')
def api_marketplace_recipe_delete(request):
    '''
    .. http:delete:: /api/1.0/marketplace/recipe/{str:recipe}

        Delete a owned recipe

       :statuscode 200: no error
       :statuscode 403: not authorized
       :statuscode 404: not found
    '''
    recipe_id = request.matchdict['recipe']
    recipe_filter = {'id': recipe_id}
    user = is_authenticated(request)
    if not user:
        return HTTPForbidden()
    recipe = request.registry.db_mongo['recipes'].find_one(recipe_filter)
    if recipe:
        if not recipe['visible']:
            if recipe['owner'] != user['id']:
                return HTTPForbidden()
        request.registry.db_mongo['recipes'].delete(recipe_filter)
        return {'msg': 'recipe deleted'}
    else:
        return HTTPNotFound()
