from pyramid.config import Configurator
from pyramid.renderers import JSON
from pyramid.events import BeforeRender
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid_beaker import session_factory_from_settings


import os
import sys
import json
import datetime
import logging
from bson import json_util
from bson.objectid import ObjectId
import yaml
import socket
from pymongo import MongoClient
import redis

from yapsy.PluginManager import PluginManager
from godocker.iAuthPlugin import IAuthPlugin
from godocker.iSchedulerPlugin import ISchedulerPlugin
from godocker.iExecutorPlugin import IExecutorPlugin
from godocker.iStatusPlugin import IStatusPlugin

from godocker.godscheduler import GoDScheduler

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    global_properties = settings.get('global_properties', 'go-d.ini')
    if not os.path.exists(global_properties):
      print('go-d.ini configuration field is not set')
      sys.exit(1)

    settings['global_properties'] = global_properties


    config = Configurator(settings=settings)
    #config.include('pyramid_chameleon')

    cfg= None
    with open(global_properties, 'r') as ymlfile:
        cfg = yaml.load(ymlfile)

    social_providers = []
    if 'allow_auth' in config.registry.settings and cfg['guest_allow']:
        social_providers = config.registry.settings['allow_auth'].split(',')
    if 'google' in social_providers:
        config.include('velruse.providers.google_oauth2')
        config.add_google_oauth2_login_from_settings()
    if 'github' in social_providers:
        config.include('velruse.providers.github')
        config.add_github_login_from_settings()
    config.add_subscriber(before_render, BeforeRender)


    config.registry.god_config = cfg
    config.registry.p_pending = None
    config.registry.p_running = None
    config.registry.p_total = None
    config.registry.p_api_requests = None
    config.registry.p_exporter = {}
    config.registry.allow_auth = social_providers


    my_session_factory = session_factory_from_settings(settings)
    config.set_session_factory(my_session_factory)

    if not cfg['plugins_dir']:
        import tempfile
        pid_file = tempfile.mkstemp()
        gs = GoDScheduler(pid_file)
        gs.load_config(global_properties)
        gs.stop_daemon = False
        cfg['plugins_dir'] = gs.cfg['plugins_dir']
        gs = None
    print("Plugin directory: %s" % cfg['plugins_dir'])
    # Build the manager
    simplePluginManager = PluginManager()
    # Tell it the default place(s) where to find plugins
    simplePluginManager.setPluginPlaces([cfg['plugins_dir']])
    simplePluginManager.setCategoriesFilter({
               "Scheduler": ISchedulerPlugin,
               "Executor": IExecutorPlugin,
               "Auth": IAuthPlugin,
               "Status": IStatusPlugin
     })
    # Load all plugins
    simplePluginManager.collectPlugins()


    gologger = logging.getLogger('godweb')

    mongo = MongoClient(cfg['mongo_url'])
    db = mongo[cfg['mongo_db']]
    config.registry.db_mongo =db


    auth_policy = None
    # Activate plugins

    status_manager = None
    for pluginInfo in simplePluginManager.getPluginsOfCategory("Status"):
        if 'status_policy' not in cfg or not cfg['status_policy']:
            print("No status manager in configuration")
            break
        if pluginInfo.plugin_object.get_name() == cfg['status_policy']:
            status_manager = pluginInfo.plugin_object
            status_manager.set_config(cfg)
            status_manager.set_logger(gologger)
            print("Loading status manager: "+status_manager.get_name())
    config.registry.status_manager = status_manager

    for pluginInfo in simplePluginManager.getPluginsOfCategory("Auth"):
        if pluginInfo.plugin_object.get_name() == cfg['auth_policy']:
             auth_policy = pluginInfo.plugin_object
             auth_policy.set_users_handler(db.users)
             auth_policy.set_config(cfg)
             auth_policy.set_logger(gologger)
             print("Loading auth policy: "+auth_policy.get_name())
    config.registry.auth_policy = auth_policy

    executor = None
    for pluginInfo in simplePluginManager.getPluginsOfCategory("Executor"):
        if pluginInfo.plugin_object.get_name() == cfg['executor']:
             executor = pluginInfo.plugin_object
             executor.set_config(cfg)
             executor.set_logger(gologger)
             print("Loading executor: "+executor.get_name())

    config.registry.executor = executor


    r = redis.StrictRedis(host=cfg['redis_host'], port=cfg['redis_port'], db=cfg['redis_db'])

    config.registry.db_redis = r

    authentication_policy = AuthTktAuthenticationPolicy('seekrit',
        callback=None, hashalg='sha512', timeout=3600*24)
    authorization_policy = ACLAuthorizationPolicy()

    config.set_authentication_policy(authentication_policy)
    config.set_authorization_policy(authorization_policy)


    config.add_static_view('static', 'static', cache_max_age=3600)
    pyramid_env = os.environ.get('PYRAMID_ENV', 'dev')
    if pyramid_env == 'prod':
        config.registry.runenv = 'dist'
        config.add_static_view('app', 'godweb:webapp/dist/')
    else:
        config.registry.runenv = 'app'
        config.add_static_view('app', 'godweb:webapp/app/')
    config.add_route('home', '/')
    config.add_route('user_list', '/api/1.0/user')
    config.add_route('user_info', '/api/1.0/user/{id}')
    config.add_route('user_usage', '/api/1.0/user/{id}/usage')
    config.add_route('user_logged', '/user/logged')
    config.add_route('user_bind', '/user/bind')
    config.add_route('guest_bind', '/guest/bind')
    config.add_route('guest_status', '/guest/status')
    config.add_route('user_logout', '/user/logout')
    config.add_route('admin_status', '/admin/status')
    config.add_route('admin_maintenance', '/admin/maintenance')
    config.add_route('admin_set_maintenance', '/admin/maintenance/{node}/{status}')
    config.add_route('admin_unwatch', '/admin/proc/{id}')

    config.add_route('api_ping', '/api/1.0/ping')
    config.add_route('api_config', '/api/1.0/config')
    config.add_route('api_usage', '/api/1.0/usage')
    config.add_route('api_images', '/api/1.0/image')
    config.add_route('api_image_count', '/api/1.0/image/{image}')
    config.add_route('api_tasks', '/api/1.0/task')
    config.add_route('api_auth', '/api/1.0/authenticate')
    config.add_route('api_prometheus', '/api/1.0/prometheus')
    config.add_route('api_count','/api/1.0/task/status/{status}/count')
    config.add_route('api_task_active', '/api/1.0/task/active')
    config.add_route('api_task_active_all', '/api/1.0/task/active/all')
    config.add_route('api_task_over', '/api/1.0/task/over')
    config.add_route('api_task_over_all', '/api/1.0/task/over/all')
    config.add_route('api_task_over_cursor', '/api/1.0/task/over/{skip}/{limit}')
    config.add_route('api_task_over_all_cursor', '/api/1.0/task/over/all/{skip}/{limit}')
    config.add_route('api_task', '/api/1.0/task/{task}')
    config.add_route('api_task_status', '/api/1.0/task/{task}/status')
    config.add_route('api_task_monitor', '/api/1.0/task/{task}/monitor')
    config.add_route('api_task_pending', '/api/1.0/task/{task}/pending')
    config.add_route('api_task_suspend', '/api/1.0/task/{task}/suspend')
    config.add_route('api_task_reschedule', '/api/1.0/task/{task}/reschedule')
    config.add_route('api_task_resume', '/api/1.0/task/{task}/resume')
    config.add_route('api_task_token', '/api/1.0/task/{task}/token')
    config.add_route('api_task_file', '/api/1.0/task/{task}/files*file')
    config.add_route('api_user_project', '/api/1.0/user/{id}/project')
    config.add_route('api_user_task_delete', '/api/1.0/user/{id}/task')
    config.add_route('api_user_apikey', '/api/1.0/user/{id}/apikey')
    config.add_route('api_project_list', '/api/1.0/project')
    config.add_route('api_project', '/api/1.0/project/{project}')
    config.add_route('api_project_usage', '/api/1.0/project/{project}/usage')
    config.add_route('api_marketplace_recipes', '/api/1.0/marketplace/recipe')
    config.add_route('api_marketplace_recipe', '/api/1.0/marketplace/recipe/{recipe}')
    config.add_route('prometheus', '/metrics')


    config.scan()


    # automatically serialize bson ObjectId and datetime to Mongo extended JSON
    json_renderer = JSON()
    def pymongo_adapter(obj, request):
        return json_util.default(obj)
    json_renderer.add_adapter(ObjectId, pymongo_adapter)
    json_renderer.add_adapter(datetime.datetime, pymongo_adapter)
    def py3bytes_adapter(obj, request):
        return obj.decode()
    if sys.version_info >= (3,):
        json_renderer.add_adapter(bytes, py3bytes_adapter)

    config.add_renderer('json', json_renderer)

    status_manager = config.registry.status_manager
    if status_manager is not None:
        hostname = socket.gethostbyaddr(socket.gethostname())[0]
        proc_name = 'web-'+hostname
        status_manager.keep_alive(proc_name, 'web')


    return config.make_wsgi_app()

def before_render(event):
    event["username"] = event['request'].authenticated_userid
