.. _views:


*****
GO-D-Docker WEB API
*****

Task object example:

.. code-block:: python

    task = {
        'id': task_id,  # Must not be provided in POST request to create a task
        'user': { # Filled by Auth plugin, must not be provided in POST request to create a task
            'id': 'user_id',
            'uid': 1001,
            'gid': 1001,
            'project': 'default' # project assigned to the task, if none in particular, use 'default'
        },
        'notify': {
            'email': false  # Optional email notification on task status modification (running, over, ...)
        },
        'date': time.mktime(dt.timetuple()), # Must not be provided in POST request to create a task
        'meta': {
            'name': 'some_name',
            'description': 'blabla'
        },
        'requirements': {
            'cpu': 1,
            # In Gb
            'ram': 1,
            'array': {
                'values': None  # Job arrays  start:end:step
            },
            'label': ['storage==ssd'] or None, # See Docker labels
            'tasks': [] # Optional, Ids of parent tasks, current task will not be scheduled before for the end of parent task,
            'ports': [] # Optional, list of ports to open
        },
        'container': {
            'image': 'centos:latest',
            'volumes': [],
            'network': True,
            'id': None,
            'meta': None, # Contains meta information provided by executor (hostname, ...)
            'stats': None,
            'ports': [], # Will be automatically filled for interactive jobs by mapped ports
            'root': False
        },
        'command': {
            'interactive': False,
            'cmd': '/bin/sleep 30'
        },
        'status': {
            'primary': None,
            'secondary': None
        }
    }

Task schema for task creation is available in source directory at doc/godocker.json


Go-d-docker web API reference
==================
 .. automodule:: godweb.views
   :members:
   :special-members:
